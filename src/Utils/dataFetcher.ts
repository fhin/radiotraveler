import AssetProvider from "./assetProvider";
import WeatherForecast from "./ResponseModels/weatherForecast";
import defaultWeatherIcon from "../Assets/logo512.png";

enum RequestType {
    None,
    WeatherForLocation,
    WeatherForCoordinates
};

enum UnitType {
    METRIC,
    IMPERIAL
};


class DataFetcher {
    private readonly apiEndPoint: string;
    private readonly requestUrls: { [rType: string] : string};
    private readonly unitType: UnitType;
    private readonly assetProvider: AssetProvider;
    public static readonly MAX_NUM_LOCATION_IMGS: number = 5;
    public static readonly MAX_DAYS_WEATHER_INFO: number = 7;

    constructor(){
        this.apiEndPoint = "";
        this.requestUrls = {};
        this.unitType = UnitType.METRIC;
        this.assetProvider = new AssetProvider();
    }


    fetchImagesForLocationName(locName: string) : Promise<string[]> {
        const imagesAsStrings = new Promise<string[]>((resolve, reject) => {
            const imageSources = new Array(DataFetcher.MAX_NUM_LOCATION_IMGS);
            for (var i = 0; i < DataFetcher.MAX_NUM_LOCATION_IMGS; i++){
                imageSources[i] = this.assetProvider.loadEncodedLocPlaceHolder();
            }
            resolve(imageSources);
        });
        return imagesAsStrings;
    }

    fetchImagesForLocationCoords(locLatitude: number, locLongitude: number) : Promise<string[]>{
        const imagesAsStrings = new Promise<string[]>((resolve, reject) => {
            const imageSources = new Array(DataFetcher.MAX_NUM_LOCATION_IMGS);
            for (var i = 0; i < DataFetcher.MAX_NUM_LOCATION_IMGS; i++){
                imageSources[i] = this.assetProvider.loadEncodedLocPlaceHolder();
            }
            resolve(imageSources);
        });
        return imagesAsStrings;
    }

    private generateRandomWeatherData() : WeatherForecast {
        const date = new Date();
        const temperature_min = Math.round(Math.random() * 100);
        const temperature_max = Math.round(Math.random() * 100);
        const unitType = Math.random() > 0.5 ? UnitType.IMPERIAL : UnitType.METRIC;
        //const feelLikeTemp = Math.round(temperature_min * 1.4);
        const rainfall = 50;
        const humidity = Math.random() * 100;
        const mainWeather = "main";
        const description = "Test description";
        const icon = defaultWeatherIcon;
        const wData = new WeatherForecast(date, temperature_min, temperature_max, unitType, 
            rainfall, humidity, mainWeather, description, icon);
        return wData;
    }

    /*  WEATHER API, see: https://openweathermap.org/api/one-call-api 
    */
    fetchWeatherDataForLocation(cityName: string, onlyToday: boolean) : Promise<WeatherForecast[]> {
        var weatherData = new Array<WeatherForecast>(onlyToday ? 1 : 7);
        console.log("FETCH for location");
        for (var i = 0; i < 7; i++){
            weatherData[i] = this.generateRandomWeatherData();
            weatherData[i].date.setDate(weatherData[i].date.getDate() + i);
        }
        return new Promise((resolve, reject) => resolve(weatherData));
    }

    fetchWeatherDataForCoordinates(latitude: number, longitude: number, onlyToday: boolean) : Promise<WeatherForecast[]>{
        console.log("FETCH for coords");
        var weatherData = new Array<WeatherForecast>(onlyToday ? 1 : 7);
        for (var i = 0; i < 7; i++){
            weatherData[i] = this.generateRandomWeatherData();
        }
        return new Promise((resolve, reject) => resolve(weatherData));
    }

    private requestUrlLookup(requestType: RequestType) : string {
        return "";
    }
}
export default DataFetcher;