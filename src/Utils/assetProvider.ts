import placeHolderImg from '../Assets/logo192.png';
import weatherPlaceholderImg from '../Assets/logo512.png';

class AssetProvider {
    private encodedLocPlaceHolderImg : string;
    private decodedLocPlaceHolderImg: string;
    private decodedWeatherPlaceHolderImg: string;
    private formatPattern : string;
    private formatRegex : RegExp;

    constructor(){
        this.formatPattern = "^data:image/(png|jpg|jpeg|gif|(svg+xml));base64,";
        this.formatRegex = new RegExp(this.formatPattern);
        this.encodedLocPlaceHolderImg = "";
        this.decodedLocPlaceHolderImg = "";
        this.decodedWeatherPlaceHolderImg = "";
        this.getPlaceHolderData();
    }

    private getPlaceHolderData() : void {
        try {
            if (!this.formatRegex.test(placeHolderImg)){
                throw new Error("Imported placeholder image string does not match pattern !");
            }
            this.decodedWeatherPlaceHolderImg = weatherPlaceholderImg;
            this.decodedLocPlaceHolderImg = placeHolderImg;
            this.encodedLocPlaceHolderImg = btoa(placeHolderImg);
        }
        catch (err){
            console.log("Error fetching placeholder img data");
        }
    }

    loadEncodedLocPlaceHolder() : string {
        return this.encodedLocPlaceHolderImg;
    }

    loadDecodedLocPlaceHolder() : string {
        return this.decodedLocPlaceHolderImg;
    }

    loadDecodedWeatherPlaceHolderIcon() : string {
        return this.decodedWeatherPlaceHolderImg;
    }
}
export default AssetProvider;