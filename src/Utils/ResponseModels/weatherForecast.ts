import RequestConstants from "../Request/requestConstants";

export enum UnitType {
    IMPERIAL,
    METRIC,
    KELVIN
};

class WeatherForecast {
    date: Date;
    temperature_min: number;
    temperature_max: number;
    unitType: UnitType;
    rainfall: number;
    humidity: number;
    mainWeather: string;
    description: string;
    icon: string;

    constructor(date: Date, temperature_min: number, temperature_max: number, unitType: UnitType, rainfall: number, humidity: number,
        mainWeather: string, description: string, icon:string) {
        this.date = date;
        this.temperature_min = temperature_min;
        this.temperature_max = temperature_max;
        this.unitType = unitType;
        this.rainfall = rainfall;
        this.humidity = humidity;
        this.mainWeather = mainWeather;
        this.description = description;
        this.icon = icon;
    }

    static createFromObject(jsonObject : any, isDailyForecast : boolean) : WeatherForecast {
        try {
            const date : Date = new Date(jsonObject[RequestConstants.WEATHER_DATE_KEY]);
            const unitType : number = parseInt(jsonObject[RequestConstants.WEATHER_RESPONSE_UNIT_KEY]);
            let unit;
            switch (unitType){
                case UnitType.IMPERIAL:
                    unit = UnitType.IMPERIAL;
                    break;
                case UnitType.METRIC:
                    unit = UnitType.METRIC;
                    break;
                case UnitType.KELVIN:
                    unit = UnitType.KELVIN;
                    break;
                default:
                    throw new Error("Unrecognized unit type !");
            }

            const minTemp : number = parseInt(jsonObject[RequestConstants.WEATHER_MINTEMP_KEY]);
            const maxTemp : number = parseInt(jsonObject[RequestConstants.WEATHER_MAXTEMP_KEY]);
            const humidity : number = parseInt(jsonObject[RequestConstants.WEATHER_HUMIDITY_KEY]);
            const generalWeather : string = jsonObject[RequestConstants.WEATHER_GENERAL_KEY];
            const weatherDescr : string = jsonObject[RequestConstants.WEATHER_DESCR_KEY];
            // TODO: Prefix for base64 string
            const icon : string = "data:image/png;base64," + jsonObject[RequestConstants.WEATHER_ICON_KEY];
            let rainfall;
            if (isDailyForecast){
                rainfall = parseInt(jsonObject[RequestConstants.WEATHER_RAINFALL_KEY]);
            }
            else {
                try {
                    rainfall = parseInt(jsonObject[RequestConstants.WEATHER_RAINFALL_KEY]);
                }
                catch (err){
                    rainfall = 0;
                }
            }
            return new WeatherForecast(date, minTemp, maxTemp, unit, rainfall, humidity, generalWeather, weatherDescr, icon);
        }
        catch (e){
            console.log("Error converting json to radio station: ", e);
            throw new Error("Error converting json to radio station !");
        }
    }

    private isWeatherData(obj : any, isDailyForecast : boolean) : boolean {
        try {
            const isWeatherData : boolean = RequestConstants.WEATHER_DATE_KEY in obj &&
                RequestConstants.WEATHER_MINTEMP_KEY in obj &&
                RequestConstants.WEATHER_MAXTEMP_KEY in obj &&
                RequestConstants.WEATHER_RESPONSE_UNIT_KEY in obj &&
                RequestConstants.WEATHER_HUMIDITY_KEY in obj &&
                RequestConstants.WEATHER_GENERAL_KEY in obj &&
                RequestConstants.WEATHER_DESCR_KEY in obj &&
                RequestConstants.WEATHER_ICON_KEY in obj;
            if (isDailyForecast){
                return isWeatherData && RequestConstants.WEATHER_RAINFALL_KEY in obj;
            }
            else {
                return isWeatherData;
            }
        }
        catch (err){

        }
        finally {
            return false;
        }
    }
}
export default WeatherForecast;