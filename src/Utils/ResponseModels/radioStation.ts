import RequestConstants from '../Request/requestConstants';

class RadioStation {
    public id: string;
    public name: string;
    public latitude: number;
    public longitude: number;
    public audioSource: string;
    public countryName: string;
    public cityName: string;

    constructor(id: string, name:string, latitude: number, longitude: number, audioSource: string, countryName: string, cityName: string){
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.audioSource = audioSource;
        this.countryName = countryName;
        this.cityName = cityName;
    }   

    static createFromJson(jsonText: string) : RadioStation {
        try {
            var responseObj = JSON.parse(jsonText);
            if (!this.isRadioStation(responseObj)){
                throw new Error("Given object cannot be converted to radioStation !");
            }
            return this.createFromObject(responseObj);
        }
        catch (e){
            console.log("Error converting json to radio station: ", e);
            throw new Error("Error converting json to radio station !");
        }
    }

    static createFromObject(jsonObj: any) : RadioStation {
        try {
            const responseObj = jsonObj;
            const id : string = responseObj[RequestConstants.RADIO_RESPONSE_ID_KEY];
            const name : string = responseObj[RequestConstants.RADIO_RESPONSE_NAME_KEY];
            const latitude : number = parseFloat(parseFloat(responseObj[RequestConstants.RADIO_RESPONSE_LATITUDE_KEY]).toFixed(3));
            const longitude : number = parseFloat(parseFloat(responseObj[RequestConstants.RADIO_RESPONSE_LONGITUDE_KEY]).toFixed(3));
            const audioSource : string = responseObj[RequestConstants.RADIO_RESPONSE_AUDIOSOURCEURI_KEY];
            const countryName : string = responseObj[RequestConstants.RADIO_RESPONSE_COUNTRYNAME_KEY];
            const cityName :string = responseObj[RequestConstants.RADIO_RESPONSE_CITYNAME_KEY];
            return new RadioStation(id, name, latitude, longitude, audioSource, countryName, cityName)
        }
        catch (e){
            console.log("Error converting json to radio station: ", e);
            throw new Error("Error converting json to radio station !");
        }
    }

    /*
    static isRadioStation(o: any) : o is RadioStationType {
        return "test" in o;
    }
    */
    static isRadioStation(o: any) : boolean {
        try {
            return RequestConstants.RADIO_RESPONSE_ID_KEY in o && 
            RequestConstants.RADIO_RESPONSE_NAME_KEY in o &&
            RequestConstants.RADIO_RESPONSE_LATITUDE_KEY in o &&
            RequestConstants.RADIO_RESPONSE_LONGITUDE_KEY in o &&
            RequestConstants.RADIO_RESPONSE_CITYNAME_KEY in o &&
            RequestConstants.RADIO_RESPONSE_COUNTRYNAME_KEY in o &&
            RequestConstants.RADIO_RESPONSE_AUDIOSOURCEURI_KEY in o;
        }
        catch (e){
            console.log("Error checking if object is a radioStation: ", e);
        }
        finally {
            return false;
        }
    }
}
export default RadioStation;