import RequestConstants from '../Request/requestConstants';

export type SectionType = {
    sectionName: string,
    sectionText: string
};

type ResponseSectionType = {
    Item1: string,
    Item2: string
};

class CityInformation {
    public cityName : string;
    public countryName : string;
    public summary : string;
    public sections : Array<SectionType>;

    constructor(cityName: string, countryName: string, summary: string, sections: Array<SectionType>){
        this.cityName = cityName;
        this.countryName = countryName;
        this.summary = summary;
        this.sections = sections;
    }

    static createFromObject(jsonObj : any) : CityInformation {
        try {
            /*
            if (!this.isCityInformation(jsonObj)){
                throw new Error("Given object is not a city information !");
            }
            */
            const responseObj = jsonObj;
            const cityName : string = responseObj[RequestConstants.CITYINFO_CITYNAME_KEY];
            const countryName : string = responseObj[RequestConstants.CITYINFO_COUNTRYNAME_KEY];
            const summary : string = responseObj[RequestConstants.CITYINFO_SUMMARY_KEY];
            const responseSections : Array<ResponseSectionType> = responseObj[RequestConstants.CITYINFO_SECTIONS_KEY];
            var sections = new Array<SectionType>(responseSections.length);
            responseSections.forEach((section, idx) => {
                var mappedSection : SectionType = {
                    sectionName: section.Item1,
                    sectionText: section.Item2
                }
                sections[idx] = mappedSection;
            });
            return new CityInformation(cityName, countryName, summary, sections);
        }
        catch (err) {
            console.log("Error converting json to city information: ", err);
            throw new Error("Error converting json to city information !");
        }
    }

    private static isCityInformation(obj : any) : boolean {
        try {
            const isCityInformation : boolean = RequestConstants.CITYINFO_CITYNAME_KEY in obj &&
                RequestConstants.CITYINFO_COUNTRYNAME_KEY in obj &&
                RequestConstants.CITYINFO_SUMMARY_KEY in obj &&
                RequestConstants.CITYINFO_SECTIONS_KEY in obj;
            return isCityInformation;
        }
        catch (err){

        }
        finally {
            return false;
        }
    }
}
export default CityInformation;