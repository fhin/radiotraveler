class Dictionary {
    private _keys: string[];
    private _keySet: Set<string>;
    private _values: any[];

    constructor(){
        this._keys = new Array<string>(0);
        this._values = new Array<any>(0);
        this._keySet = new Set<string>();
    }

    containsKey(key: string) : boolean {
        return this._keySet.has(key);
    }

    private findKeyIndex(key:string) : number {
        if (!this._keySet.has(key)) return -1;
        return this._keys.indexOf(key);
    }

    add(key: string, value: any){
        if (this.containsKey(key)) {
            this._keys[this.findKeyIndex(key)] = value;
        }
        else {
            this._keys.push(key);
            this._values.push(value);
            this._keySet.add(key);
        }
    }

    remove(key: string) {
        var keyIndex = this.findKeyIndex(key);
        if (keyIndex === -1) return;
        this._keys.slice(keyIndex, 1);
        this._values.slice(keyIndex, 1);
        this._keySet.delete(key);
    }

    get(key: string) : any {
        var idx = this.findKeyIndex(key);
        if (idx === -1) return null;
        else return this._values[idx];
    }

    numEntries() : number {
        return this._keys.length;
    }
}
export default Dictionary;