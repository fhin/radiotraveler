export enum RequestType {
    RANDOM_RADIO_STATIONS,
    DAILY_WEATHER_FORECAST,
    HOURLY_WEATHER_FORECAST,
    CITY_INFORMATION
}

class RequestConstants {
    public static MAX_RANDOM_RADIO_STATIONS: number = 5;
    public static MAX_FORECASTS_PER_FETCH: number = 7;

    public static HTTP_FIRST_PARAM_PREFIX = "?";
    public static HTTP_OTHER_PARAM_PREFIX = "&";
    public static HTTP_SLASH_PARAM_PREFIX = "/";

    public static RADIO_API_ENDPOINT: string = "https://localhost:44353/api/radioStation/fetch";
    public static WEATHER_DAILY_API_ENDPOINT: string = "https://localhost:44353/api/weatherforecast/daily";
    public static WEATHER_HOURLY_API_ENDPOINT: string = "https://localhost:44353/api/weatherforecast/hourly";
    public static LOCATION_INFORMATION_API_ENDPOINT: string = "https://localhost:44353/api/cityInfo/location";

    // Radio station fetch response json field keys
    public static readonly RADIO_RESPONSE_ID_KEY = "id";
    public static readonly RADIO_RESPONSE_NAME_KEY = "stationName";
    public static readonly RADIO_RESPONSE_CITYNAME_KEY = "cityName";
    public static readonly RADIO_RESPONSE_COUNTRYNAME_KEY = "country";
    public static readonly RADIO_RESPONSE_AUDIOSOURCEURI_KEY = "audioSourceURI";
    public static readonly RADIO_RESPONSE_LATITUDE_KEY = "latitude";
    public static readonly RADIO_RESPONSE_LONGITUDE_KEY = "longitude";

    // Weather forecast fetch response json field keys
    public static readonly WEATHER_RESPONSE_UNIT_KEY = "unitType";
    public static readonly WEATHER_DATE_KEY = "date";
    public static readonly WEATHER_MINTEMP_KEY = "minTemp";
    public static readonly WEATHER_MAXTEMP_KEY = "maxTemp";
    public static readonly WEATHER_RAINFALL_KEY = "rainfall";
    public static readonly WEATHER_HUMIDITY_KEY = "humidity";
    public static readonly WEATHER_GENERAL_KEY = "generalWeather";
    public static readonly WEATHER_DESCR_KEY = "weatherDescription";
    public static readonly WEATHER_ICON_KEY = "icon";

    // City information fetch response json field keys
    public static readonly CITYINFO_CITYNAME_KEY = "cityName";
    public static readonly CITYINFO_COUNTRYNAME_KEY = "countryName";
    public static readonly CITYINFO_SUMMARY_KEY = "summary";
    public static readonly CITYINFO_SECTIONS_KEY = "sections";
    
}
export default RequestConstants;