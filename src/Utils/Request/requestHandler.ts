import RequestConstants, {RequestType} from './requestConstants';
import Request from './request';

// https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
class RequestHandler {
    private readonly BOOLEAN_TYPE = "boolean";
    private readonly STRING_TYPE = "string";
    constructor(){
    }

    getRequestForType(requestType: RequestType) : Request {
        switch (requestType){
            case RequestType.RANDOM_RADIO_STATIONS:
                return new Request(RequestConstants.RADIO_API_ENDPOINT, false);
            case RequestType.DAILY_WEATHER_FORECAST:
                const dailyWeatherRequest = new Request(RequestConstants.WEATHER_DAILY_API_ENDPOINT, false);
                dailyWeatherRequest.addFormalParameter("latitude", this.STRING_TYPE);
                dailyWeatherRequest.addFormalParameter("longitude", this.STRING_TYPE);
                dailyWeatherRequest.addFormalParameter("useMetric", this.BOOLEAN_TYPE);
                dailyWeatherRequest.fixParameterOrder([ "latitude", "longitude", "useMetric"]);
                //dailyWeatherRequest.addActualParameter("useMetric", true);
                return dailyWeatherRequest;
            case RequestType.HOURLY_WEATHER_FORECAST:    
                const hourlyWeatherRequest = new Request(RequestConstants.WEATHER_HOURLY_API_ENDPOINT, false);
                hourlyWeatherRequest.addFormalParameter("latitude", this.STRING_TYPE);
                hourlyWeatherRequest.addFormalParameter("longitude", this.STRING_TYPE);
                hourlyWeatherRequest.addFormalParameter("useMetric", this.BOOLEAN_TYPE);
                hourlyWeatherRequest.fixParameterOrder([ "latitude", "longitude", "useMetric"]);
                //hourlyWeatherRequest.addActualParameter("useMetric", true);
                return hourlyWeatherRequest;
            case RequestType.CITY_INFORMATION:
                const cityInfoRequest = new Request(RequestConstants.LOCATION_INFORMATION_API_ENDPOINT, false);
                cityInfoRequest.addFormalParameter("cityName", this.STRING_TYPE);
                cityInfoRequest.addFormalParameter("countryName", this.STRING_TYPE);
                cityInfoRequest.fixParameterOrder(["cityName", "countryName"]);
                return cityInfoRequest;
            default:
                throw new Error("No mapping for given request type");
        }
    }

    async sendRequest(request : Request) : Promise<string> {
        try {
            console.log("Sending request: ", request.encodeToURI())
            const response : string = await fetch(request.encodeToURI(), { 
                method: 'GET',
                mode: 'cors',
                credentials: 'same-origin',
                redirect: 'follow'
             })
            .then(response => response.text() )
            .catch(error => {
                console.log("Error: ", error);
                return "";
            });
            return response;
        } catch (error) {
            console.log("Error: ", error);
            throw new Error("Error while sending request: " +  error);
        }
    }
}
export default RequestHandler;