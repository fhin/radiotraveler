import Dictionary from "../dictionary";
import RequestConstants from "./requestConstants";

class Request {
    private BASE_URL: string;
    private formalParameters: Dictionary;
    private actualParameters: Dictionary;
    private parameterOrder: string[];
    private useNamedParameters: boolean;
    private parameterOrderFixed: boolean;
    private readonly validParamType : Set<string>;

    constructor(baseUrl: string, useNamedParameters: boolean){
        if (baseUrl === null || baseUrl === undefined || baseUrl === "") throw new Error("Cannot create request with empty endpoint !");
        this.BASE_URL = baseUrl;
        this.formalParameters = new Dictionary();
        this.actualParameters = new Dictionary();
        this.parameterOrder = new Array<string>(0);
        this.useNamedParameters = useNamedParameters;
        this.parameterOrderFixed = false;
        this.validParamType = new Set<string>(["boolean", "number", "string"]);
    }

    addFormalParameter(parameterName: string, parameterType: string) {
        if (parameterName === undefined || parameterName ===null || parameterName.length === 0 
            || parameterType === undefined || parameterType === null || !this.validParamType.has(parameterType)){
                throw new Error("Error adding formal parameter");
            }

        this.formalParameters.add(parameterName, parameterType);
    }

    addActualParameter(formalParameterName: string, actualParameterValue: any){
        if (!this.formalParameters.containsKey(formalParameterName)) throw new Error("Cannot add value for an unspecified formal parameter");
        var formalParamType = this.formalParameters.get(formalParameterName);
        if (formalParamType === null) throw new Error("Invalid formal parameter type");
    
        if (formalParamType === typeof actualParameterValue){
            this.actualParameters.add(formalParameterName, actualParameterValue);
            return;
        }
        throw new Error("error while adding value " + actualParameterValue + " for formal parameter " + formalParameterName);
    }

    fixParameterOrder(parameterOrder: string[]){
        if (this.parameterOrderFixed) return;
        if (parameterOrder.length !== this.formalParameters.numEntries()) throw new Error("Given parameter count does not match its formal one !");

        var tmpOrder = new Array<string>(this.formalParameters.numEntries());
        var numParams = this.formalParameters.numEntries();
        var currParam : string;
        for (var i = 0; i < numParams; i++){
            currParam = parameterOrder[i];
            if (currParam === undefined || currParam === null || currParam === "") throw new Error("Invalid parameter name !");
            if (!this.formalParameters.containsKey(currParam)) throw new Error("There is no formal parameter with that name !");
            tmpOrder[i] = currParam;
        }
        this.parameterOrder = tmpOrder;
    }

    encodeToURI() : string {
        var delimiter = this.useNamedParameters ? RequestConstants.HTTP_FIRST_PARAM_PREFIX : RequestConstants.HTTP_SLASH_PARAM_PREFIX;
        try {
            return this.parameterOrder.map<[string, string]>(parameter => [parameter, this.actualParameters.get(parameter)])
                .reduce((accumulator, currParamVal : [string, string], currIdx) => {
                    var paramAsString : string = delimiter;
                    if (this.useNamedParameters) {
                        paramAsString = currIdx > 0 ? RequestConstants.HTTP_OTHER_PARAM_PREFIX 
                            : RequestConstants.HTTP_FIRST_PARAM_PREFIX;
                        paramAsString += currParamVal[0] + "=";
                    }
                    paramAsString += currParamVal[1].toString();
                    return accumulator + paramAsString;
            }, this.BASE_URL);

        }
        catch (e){
            console.log("Error encoding request, reason: " + e);
            throw new Error("");
        }
    }
}
export default Request;