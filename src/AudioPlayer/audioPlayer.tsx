import * as React from 'react';
import { IconButton, SvgIcon, Slider } from '@material-ui/core';
import PlayCircleFilledIcon from '@material-ui/icons/PlayCircleFilled';
import StopIcon from '@material-ui/icons/Stop';
import SkipNextIcon from '@material-ui/icons/SkipNext';
import SkipPreviousIcon from '@material-ui/icons/SkipPrevious';

import MusicNoteIcon from '@material-ui/icons/MusicNote';
import HourglassEmptyIcon from '@material-ui/icons/HourglassEmpty';
import PauseIcon from '@material-ui/icons/Pause';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';

import AudioTrack from "./audioTrack";
import './audioPlayer.css';

export enum AudioPlayerType {
    NORMAL,
    NO_INTERPRETER_INFO
}

type AudioPlayerProperties = {
    audioPlayerType: AudioPlayerType,
    audioTracks: Array<AudioTrack>,
	currAudioSourceIdx: number,
    defaultVolume: number,
    onAudioIdxChange : any
};

type AudioPlayerState = {
    audioState: AUDIO_STATE;
    infoMsg: string;
    audioSource: string;
    volume: number;
}

enum AUDIO_STATE {
    LOADING,
	PLAYING,
	PAUSED
}

enum INFO_MSG {
    LOADING = "LOADING...",
    PLAYING = "PLAYING",
    PAUSED = "PAUSED",
    READY_TO_PLAY = "PRESS START TO PLAY"
}

class AudioPlayer extends React.Component<AudioPlayerProperties, AudioPlayerState> {
    static defaultProps = {
		audioSource: "http://rdst.win:7845/stream?listening-from-radio-garden=1618503501342",
        defaultVolume: .75,
        displayInfos: true
    }
    
    audioElement: HTMLAudioElement;
    constructor(props: AudioPlayerProperties){
        super(props);
        const audioSource = decodeURIComponent(props.audioTracks[this.props.currAudioSourceIdx].audioSource);
		this.state = { 
            audioState: AUDIO_STATE.PAUSED,
            infoMsg: INFO_MSG.LOADING, 
            audioSource: audioSource,
            volume: this.props.defaultVolume
        };
        this.audioElement = new Audio();
    }

    componentDidMount(){
        this.audioElement.addEventListener("canplay", event => {
            if (this.audioElement.canPlayType('audio/mp3') !== ""){
                // Client can play audio
                var autoPlayPromise = this.audioElement.play();
                // Check if auto play is allowed, see further: https://developers.google.com/web/updates/2017/09/autoplay-policy-changes
                if (autoPlayPromise !== undefined) {
                    autoPlayPromise.then(_ => {
                        // Autoplay started!
                        console.log("Auto play allowed")
                        this.setState({
                            audioState: AUDIO_STATE.PLAYING,
                            audioSource: this.audioElement.src,
                            infoMsg: INFO_MSG.PLAYING
                        });
                    }).catch(error => {
                        // Autoplay was prevented.
                        // Show a "Play" button so that user can start playback.
                        console.log("Auto play not allowed");
                        this.setState({
                            audioState: AUDIO_STATE.PAUSED,
                            audioSource: this.audioElement.src,
                            infoMsg: INFO_MSG.READY_TO_PLAY
                        });
                    });
                }
            }
            else {
                alert("It does seem like your browser does not support playing mp3 files !");
            }
        });

        /*
        this.audioElement.addEventListener("canplaythrough", event => {
            console.log("test");
        });
        */
        this.loadNewAudio(this.state.audioSource);
    }

    componentWillUnmount(){
        this.audioElement.pause();
    }

    playAudio(){
        if (this.state.audioState !== AUDIO_STATE.PAUSED) return;

        this.audioElement.play();
        this.setState({
            audioState: AUDIO_STATE.PLAYING,
            infoMsg: INFO_MSG.PLAYING
        });
    }

    pauseAudio() {
        if (this.state.audioState !== AUDIO_STATE.PLAYING) return;

        this.audioElement.pause();
        this.setState({
            audioState: AUDIO_STATE.PAUSED,
            infoMsg: INFO_MSG.PAUSED
        });
    }

    changeVolume(event: any, newValue: number | number[]){
        var newVolume = this.props.defaultVolume;
		if (typeof newValue === "number") {
			newVolume = newValue;
		}
		else {
			newVolume = newValue[newValue.length - 1];
		}
		if (newValue < 0 || newValue > 1) {
			newValue = this.props.defaultVolume;
        }
        this.audioElement.volume = newVolume;
        this.setState({
            volume: newVolume
        });
    }

    loadNewAudio(audioSource: string){
        if (!audioSource) return;
        this.audioElement.src = audioSource;
        this.audioElement.load();
    }

    private switchAudioSrc(currentIdx : number, switchToPrevious: boolean) {
        const maxSrcIdx = this.props.audioTracks.length - 1;
        var newSrcIdx = currentIdx;
        if (newSrcIdx === -1){
            console.log("Cannot change station since there were no sources available !");
            return;
        } 

        if (switchToPrevious){
            if (newSrcIdx - 1 < 0){
                newSrcIdx = maxSrcIdx;
            }
            else {
                newSrcIdx--;
            }
        }
        else {    
            if (newSrcIdx + 1 > maxSrcIdx){
                newSrcIdx = 0;
            }
            else {
                newSrcIdx++;
            }
        }
        this.pauseAudio();
        console.log("Changing station !");
        this.props.onAudioIdxChange(newSrcIdx);
        this.loadNewAudio(decodeURIComponent(this.props.audioTracks[newSrcIdx].audioSource));
    }

    loadPrevStation() {
        console.log("Loading prev station !");
        this.switchAudioSrc(this.props.currAudioSourceIdx, true);
    }

    loadNextStation() {
        console.log("Loading next station !");
        this.switchAudioSrc(this.props.currAudioSourceIdx, true);
    }

    getInfoIcon(audioState : AUDIO_STATE){
        switch (audioState){
            case AUDIO_STATE.PLAYING:
                return MusicNoteIcon;
            case AUDIO_STATE.LOADING:
                return HourglassEmptyIcon;
            case AUDIO_STATE.PAUSED:
                return PauseIcon;
            default:
                return ErrorOutlineIcon;
        }
    }

    render(){
        //this.loadNewAudio(this.props.audioSources[this.props.currAudioSourceIdx]);
        const playing = this.state.audioState === AUDIO_STATE.PLAYING;
        const currTrack = this.props.audioTracks[this.props.currAudioSourceIdx];
        const playerType = this.props.audioPlayerType;
        const onlyControls : boolean = this.props.audioPlayerType === AudioPlayerType.NO_INTERPRETER_INFO;
        let mediaControlIcon = !playing ? PlayCircleFilledIcon : StopIcon;
        
        const audioIconStyleClass = onlyControls ? "audioIconNoInfo" : "audioIcon";
        const interpreterStyleClass = onlyControls ? "interpreterNoInfo" : "interpreterInfo";
        const audioControlsStyleClass = onlyControls ? "audioControlsNoInfo" : "audioControls";

        return (
            <div className="anotherAudioPlayerWrapper">
                <span className="trackInfoSpan">{!onlyControls && currTrack.recordName + " | " + currTrack.interpreter}</span>
                <div className="audioPlayer">
                    <div className={audioIconStyleClass}>
                        <IconButton aria-label="audioIcon" disabled={true}>
                            <SvgIcon component={this.getInfoIcon(this.state.audioState)} 
                                style={{ fontSize: 44 }}/>
                        </IconButton>
                    </div>
                    
                    <div className={interpreterStyleClass}>
                        {playerType === AudioPlayerType.NORMAL &&
                            <div>
                                <p>{currTrack.recordName}</p>
                                <p>{currTrack.interpreter}</p>
                            </div>
                        }
                    </div>
                    <div className={audioControlsStyleClass}>
                        <div className="audioSlider">
                            {playing && 
                                <Slider
                                    onChange={this.changeVolume.bind(this)}
                                    defaultValue={this.props.defaultVolume}
                                    value={this.state.volume}
                                    step={0.1}
                                    aria-labelledby="audio-slider"
                                    min={0}
                                    max={1}
                                /> 
                            }
                        </div>    
                        <div className="audioStateControls">
                            <IconButton aria-label="skip-prev" onClick={this.loadPrevStation.bind(this)}>
                                <SvgIcon component={SkipPreviousIcon} style={{ fontSize: 22 }} />
                            </IconButton>
                            <IconButton aria-label="play" onClick={playing ? this.pauseAudio.bind(this) : this.playAudio.bind(this)}>
                                <SvgIcon component={mediaControlIcon} style={{ fontSize: 22 }} />
                            </IconButton>
                            <IconButton aria-label="skip-next" onClick={this.loadNextStation.bind(this)}>
                                <SvgIcon component={SkipNextIcon} style={{ fontSize: 22 }} />
                            </IconButton>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default AudioPlayer;