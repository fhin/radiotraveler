class Audiotrack {
    recordName: string;
    interpreter : string;
    audioSource : string;

    constructor(recordName: string, interpreter : string, audioSource : string){
        this.recordName = recordName;
        this.interpreter = interpreter;
        this.audioSource = audioSource;
    }
}
export default Audiotrack;