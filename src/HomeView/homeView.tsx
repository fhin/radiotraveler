import * as React from 'react';
import './homeView.css';
import { Link as RouterLink } from 'react-router-dom';

import { Box, IconButton, SvgIcon } from '@material-ui/core/';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';

import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ReplayIcon from '@material-ui/icons/Replay';
import SearchIcon from '@material-ui/icons/Search';
import PublicIcon from '@material-ui/icons/Public';

import AudioPlayer from '../AudioPlayer/audioPlayer';
import { AudioPlayerType } from '../AudioPlayer/audioPlayer';
import AudioTrack from '../AudioPlayer/audioTrack';
import RequestHandler from '../Utils/Request/requestHandler';
import {RequestType} from '../Utils/Request/requestConstants';
import RadioStation from '../Utils/ResponseModels/radioStation';

type HomeProperties = {

}

type HomeState = {
	audioTracks: Array<AudioTrack>;
	currFetchedStations: Array<RadioStation>;
    audioSourcesFetched: boolean;
    currSelectedAudioIdx: number;
}

class HomeView extends React.Component<HomeProperties, HomeState> {
	private readonly audioSourceFetcher : RequestHandler;
	constructor(props: HomeProperties){
		super(props);
		this.audioSourceFetcher = new RequestHandler();

		this.state = {
			audioTracks: new Array<AudioTrack>(),
			currFetchedStations: new Array<RadioStation>(),
            audioSourcesFetched: false,
            currSelectedAudioIdx: -1
		};
        this.loadStations = this.loadStations.bind(this);
        this.handleAudioIdxChange = this.handleAudioIdxChange.bind(this);
  }

	private stationFilter<TValue>(value: TValue | null) : value is TValue {
		if (value === null || value === undefined) return false;
		return true;
	}

	private getStationsFromResponse(jsonResponseObj: any) : Promise<Array<RadioStation>> {
		const parsePromise = new Promise<Array<RadioStation>>((resolve, reject) => {
			if (jsonResponseObj === null || jsonResponseObj === undefined) reject("Cannot parse jsonResponseObj since it is null or undefined !");
			try {
				const responseArr : Array<any> = JSON.parse(jsonResponseObj);
				const fetchedStations = responseArr
					.map(responseObj => {
						try {
							return RadioStation.createFromObject(responseObj);
						}
						catch (radioStationCreationError){
							console.log("Error while converting json to radiostation !", radioStationCreationError);
							return null;
						}
					})
					.filter(this.stationFilter);
					resolve(fetchedStations);
			}
			catch (parseError){
				reject("Error parsing json response: " + parseError);
			}
		});
		return parsePromise;
	}

	async componentDidMount(){
		await this.loadStations();
	}

	private async loadStations() : Promise<void> {
		try {
			this.setState({
				audioSourcesFetched: false
			});
			var request = this.audioSourceFetcher.getRequestForType(RequestType.RANDOM_RADIO_STATIONS);
			const fetchedStations = await this.audioSourceFetcher
				.sendRequest(request)
				.then(jsonResponse => this.getStationsFromResponse(jsonResponse));
			console.log("Fetched stations !");
			
			const audioTracks = fetchedStations.map(station => {
				const titleName = station.name;
				const audioStream = station.audioSource;
				const interpreter = station.cityName + " | " + station.countryName;
				return new AudioTrack(titleName, interpreter, audioStream);
			});
			console.log("Fetched audio sources !");
			this.setState({
				currFetchedStations: fetchedStations,
				audioTracks: audioTracks,
                audioSourcesFetched: true,
                currSelectedAudioIdx: 0
			});
		}
		catch (e){
			console.log("Erorr while fetching random stations: ", e);
		}
	}

    handleAudioIdxChange(newAudioIdx: number) {
        if (newAudioIdx < 0 || newAudioIdx > this.state.audioTracks.length){
            console.log("Invalid new audio index !");
        }
        else {
            this.setState({
                currSelectedAudioIdx: newAudioIdx
            });
        }
    }

	render() {	
		const audioTracks = this.state.audioTracks;	
        const fetchedAudioSources = this.state.audioSourcesFetched;
        const currSelectedAudioIdx = this.state.currSelectedAudioIdx;
        let likeButton;
        if (fetchedAudioSources && currSelectedAudioIdx >= 0 && currSelectedAudioIdx < audioTracks.length){
            const currRadioStation = this.state.currFetchedStations[currSelectedAudioIdx];
            const latitude = currRadioStation.latitude.toString();
            const longitude = currRadioStation.longitude.toString();
            const stationName = currRadioStation.name;
            const cityName = currRadioStation.cityName;
            const countryName = currRadioStation.countryName;
            const audioStreamURI = currRadioStation.audioSource;
            const pathPrefix = "details";
            
            var paramArray : Array<string> = [stationName, cityName, countryName, latitude, longitude, audioStreamURI];
            const path = paramArray.map(param => encodeURIComponent(param)).reduce((accumulator, encodedParam) => {
                return accumulator + "/" + encodedParam
            }, pathPrefix);

            likeButton = <IconButton aria-label="like" component={RouterLink} to={path}>
                <SvgIcon component={ThumbUpIcon} style={{ fontSize: 32, fill: "lightgrey" }} />
            </IconButton>
        }
        else {
            likeButton = <IconButton aria-label="like">
                <SvgIcon component={ThumbUpIcon} style={{ fontSize: 32, fill: "lightgrey" }} />
            </IconButton>
        }
		return (
			<Box height="100%">
				<div className="mainGrid">
					<div className="mainHelpContainer">
						<IconButton aria-label="help">
							<SvgIcon component={HelpOutlineIcon} style={{ fontSize: 20, fill: "lightgrey" }} />
						</IconButton>
					</div>
					<div className="mainMediaContainer">
						<div className="mainGrid">
							<div className="logoWrapper">
								<h1>Groove'n'Move</h1>
								<SvgIcon component={PublicIcon} style={{ fontSize: 320, fill: "lightgrey" }} className="app-logo" />
							</div>
							<div className="audioPlayerWrapper">
								{fetchedAudioSources &&
									<AudioPlayer 
										audioTracks={audioTracks} 
										currAudioSourceIdx={currSelectedAudioIdx}
										onAudioIdxChange={this.handleAudioIdxChange}
										displayInfos={false}
										audioPlayerType={AudioPlayerType.NO_INTERPRETER_INFO}/>
								}
							</div>
						</div>
					</div>
					<div className="mainMediaOptsContainer">
                        {likeButton}
						<IconButton aria-label="search">
							<SvgIcon component={SearchIcon} style={{ fontSize: 32, fill: "lightgrey" }} />
						</IconButton>
						<IconButton aria-label="loadNewStations" onClick={this.loadStations}>
							<SvgIcon component={ReplayIcon} style={{ fontSize: 32, fill: "lightgrey" }} />
						</IconButton>
					</div>
				</div>
			</Box>
		);
	}
}
export default HomeView;
