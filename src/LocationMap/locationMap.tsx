import './locationMap.css'
import * as React from 'react';

import 'leaflet/dist/leaflet.css';
import { MapContainer, TileLayer, Marker, Popup} from 'react-leaflet';
import * as L from 'leaflet';

// Issue with loading markers, see: https://github.com/PaulLeCam/react-leaflet/issues/453
import icon from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';

let DefaultIcon = L.icon({
    iconUrl: icon,
    shadowUrl: iconShadow
});

L.Marker.prototype.options.icon = DefaultIcon;

type LocationMapProps = {
    latitude: number,
    longitude: number
};

type LocationMapState = {

};

class LocationMap extends React.Component<LocationMapProps, LocationMapState> {
    constructor(props : LocationMapProps){
        super(props);
    }

    render() {
        const locationTuple = L.latLng(this.props.latitude, this.props.longitude);
        return (
            <div className="locationMapWrapper">
                <MapContainer center={locationTuple} zoom={13} scrollWheelZoom={false}>
                <TileLayer
            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.osm.org/{z}/{x}/{y}.png"
          />
            <Marker position={locationTuple}>
            <Popup>
              A pretty CSS3 popup. <br /> Easily customizable.
            </Popup>
          </Marker>
                </MapContainer>
            </div>
        );
    }
}
export default LocationMap;