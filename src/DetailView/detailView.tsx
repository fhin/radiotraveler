import './detailView.css'
import * as React from 'react';
import LocationMap from '../LocationMap/locationMap';
import ImpressionViewer from '../ImpressionViewer/impressionViewer';
import AudioPlayer from '../AudioPlayer/audioPlayer';
import WeatherDetails from '../WeatherDetails/weatherDetails';
import AudioTrack from "../AudioPlayer/audioTrack";
import { AudioPlayerType } from "../AudioPlayer/audioPlayer";
import CityInfoView from '../CityInformation/cityInfoView';

import { Link, RouteComponentProps } from 'react-router-dom';
import { IconButton, SvgIcon, Button } from '@material-ui/core/';
import HomeIcon from '@material-ui/icons/Home';
import Divider from '@material-ui/core/Divider';

import Paper from '@material-ui/core/Paper';
type DetailViewerProps = {

}

type DetailViewerState = {

}

class DetailViewer extends React.Component<DetailViewerProps & RouteComponentProps, DetailViewerState> {
    private audioTracks : Array<AudioTrack>;
    private cityName: string;
    private countryName: string;
    private latitude: number;
    private longitude: number;

    constructor(props : DetailViewerProps & RouteComponentProps){
        super(props);
        const { title, cityName, countryName, latitude, longitude, audioStream } : any = this.props.match.params;

        this.cityName = cityName;
        this.countryName = countryName;
        try {
            this.latitude = parseFloat(parseFloat(latitude).toFixed(3));
            this.longitude = parseFloat(parseFloat(longitude).toFixed(3));
        }
        catch (err){
            console.error("Error while converting string coordinates into numeric values: " + err + "!\n" + 
                "Assuming default value -1 !");
            this.latitude = -1;
            this.longitude = -1;
        }
        this.audioTracks = new Array<AudioTrack>(new AudioTrack(title,cityName + "," + countryName, audioStream));
        this.handleAudioIdxChange = this.handleAudioIdxChange.bind(this);
    }

    handleAudioIdxChange(newAudioIdx: number) {
        return;
    }

    render() {
        const locationName = this.cityName + ", " + this.countryName;
        return(
            <div className="detailViewer">
            <Paper elevation={3} className="detailPaper">
                <div className="topMediaContent">
                    <div className="impressionViewer">
                        <ImpressionViewer locName={locationName} locLatitude={this.latitude} locLongitude={this.longitude} />
                    </div>
                    <div className="locationMap">
                        <LocationMap latitude={this.latitude} longitude={this.longitude}/>
                    </div>
                </div>
                <Divider orientation="vertical" flexItem className="infoWeatherVerticalDivider"/>
                <Divider orientation="horizontal" className="infoWeatherHorizonalDivider"/>
                <div className="locationDetails">
                    <div className="locationDescription">
                        <CityInfoView cityName={this.cityName} countryName={this.countryName}/>
                    </div>
                    <Divider orientation="vertical" flexItem />
                    <div className="weatherDetails">
                        <WeatherDetails locName={locationName} locLatitude={this.latitude} locLongitude={this.longitude} />
                    </div>
                </div>
                <Button variant="contained" color="default" className="bookingBtn">Search bookings</Button>
                <div className="bottomMediaContent">
                    <div className="detailAudioPlayerWrapper">
                        <AudioPlayer 
                            audioTracks={this.audioTracks} 
                            currAudioSourceIdx={0} 
                            onAudioIdxChange={this.handleAudioIdxChange}
                            audioPlayerType={AudioPlayerType.NORMAL} />
                    </div>
                    <IconButton aria-label="go home" component={Link} to="/home" className="homeBtn">
						<SvgIcon component={HomeIcon} style={{ fontSize: 20 }} />
                    </IconButton>
                </div>
            </Paper>
            </div>
        );
    }
}
export default DetailViewer;