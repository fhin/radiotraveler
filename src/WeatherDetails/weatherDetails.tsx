import * as React from 'react';
import './weatherDetails.css';
import WeatherData, { UnitType } from '../Utils/ResponseModels/weatherForecast';

import { IconButton, SvgIcon, Button } from '@material-ui/core/';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import CircularProgress from '@material-ui/core/CircularProgress';
import WeatherFetcher from './weatherFetcher';

// Weather icons
import BeachAccessIcon from '@material-ui/icons/BeachAccess';
import InvertColorsIcon from '@material-ui/icons/InvertColors';

enum WeatherLoadingState {
    LOADING,
    LOADED,
    ERROR
}

type WeatherDetailsProps = {
    locName: string,
    locLatitude: number,
    locLongitude: number
}

type WeatherDetailsState = {
    loadingStatus: WeatherLoadingState,
    weatherData: Array<WeatherData>,
    currWeatherIdx: number,
    displayDailyForecast: boolean
}

class WeatherDetails extends React.Component<WeatherDetailsProps, WeatherDetailsState> {
    private readonly dataFetcher : WeatherFetcher;
    private readonly weatherPropKeys : Array<string>;
    private readonly MAX_WEATHER_DAYS = 7;
    private readonly MAX_WEATHER_DAYS_DISPLAY = 3;

    constructor(props: WeatherDetailsProps){
        super(props);
        this.dataFetcher = new WeatherFetcher();
        this.weatherPropKeys = [ "date", "icon", "temp_min", "temp_max", "rainfall", "humidity", "mainWeather", "descr"];
        this.state = {
            currWeatherIdx: 0,
            loadingStatus: WeatherLoadingState.LOADING,
            weatherData: new Array<WeatherData>(0),
            displayDailyForecast: true
        };
    }

    async componentDidMount(){
        await this.loadWeatherData(true).catch(err => console.log("Error while fetching forecasts on startup !" + err));
    }

    private async loadWeatherData(dailyForecast : boolean) : Promise<void> {
        try {
            this.setState({
                loadingStatus: WeatherLoadingState.LOADING
            });
            var fetchedForecasts : WeatherData[] = 
                await this.dataFetcher.fetchForecasts(this.props.locLatitude, this.props.locLongitude, UnitType.METRIC, dailyForecast);
            this.setState({
                weatherData: fetchedForecasts,
                loadingStatus: WeatherLoadingState.LOADED,
                displayDailyForecast: dailyForecast
            });
        }
        catch (err){
            console.error("Error while fetching forecast !" + err);
            this.setState({
                loadingStatus: WeatherLoadingState.ERROR
            });
        }
    }

    private transformToDisplay(isDailyForecast : boolean) : Map<string, Array<string>> {
        var numItems = this.state.weatherData.length;
        var numKeys = this.weatherPropKeys.length;
        var dataDict : Map<string, Array<string>> = new Map();
        // Fill dictionary with keys
        var dataArr = new Array<Array<string>>(numKeys);
        for (var keyIdx = 0; keyIdx < numKeys; keyIdx++){
                dataDict.set(this.weatherPropKeys[keyIdx], new Array<string>(numItems));
                dataArr[keyIdx] = new Array<string>(numItems);
        }

        try {
            for (var idx = 0; idx < numItems; idx++){
                var currWeatherData = this.state.weatherData[idx];
                const tempPostfix = "°".concat(currWeatherData.unitType === UnitType.METRIC ? "C" : "F");
                if (isDailyForecast) {
                    dataArr[0][idx] = currWeatherData.date.getDate().toString() + "." + 
                        currWeatherData.date.toLocaleDateString("en-us", { weekday: 'short' }).toString();                
                }
                else {
                    dataArr[0][idx] = currWeatherData.date.getDate().toString() + "." + 
                        currWeatherData.date.toLocaleDateString("en-us", { weekday: 'short' }).toString() + 
                        " @ " + currWeatherData.date.toLocaleTimeString("en-us", { hour: "numeric" }).toString();
                }
                dataArr[1][idx] = currWeatherData.icon;
                dataArr[2][idx] = currWeatherData.temperature_min.toString().concat(tempPostfix);
                dataArr[3][idx] = currWeatherData.temperature_max.toString().concat(tempPostfix);
                dataArr[4][idx] = currWeatherData.rainfall.toString();
                dataArr[5][idx] = currWeatherData.humidity.toString();
                dataArr[6][idx] = currWeatherData.mainWeather;
                dataArr[7][idx] = currWeatherData.description;
            }

            for (keyIdx = 0; keyIdx < numKeys; keyIdx++){
                dataDict.set(this.weatherPropKeys[keyIdx], dataArr[keyIdx]);
            }
        }
        catch (err){
            console.error("Error transforming given weather data !");
        }
        return dataDict;
    }

    changeWeatherIdx(advance: boolean){
        const currWeatherIdx = this.state.currWeatherIdx;
        if ((currWeatherIdx === 0 && !advance) 
            || (currWeatherIdx === (this.MAX_WEATHER_DAYS-this.MAX_WEATHER_DAYS_DISPLAY) && advance)) return;

        this.setState({
            currWeatherIdx: advance ? currWeatherIdx +1 : currWeatherIdx -1
        });
    }

    sliceData(startIdx: number, numItems: number, maxIdx: number) : Array<number> {
        if (maxIdx < 0 || numItems < 0 || startIdx < 0) throw new Error("Cannot slice data with negative idxs !");
        if (maxIdx < startIdx) throw new Error("Cannot slice data for invalid indizes !");
        if (numItems === 0) return new Array(0);
        
        var idxArr = new Array(numItems);
        var currIdx = startIdx;
        for (var nItems = 0; nItems < numItems; nItems++){
            idxArr[nItems] = currIdx;
            currIdx++;
            if (currIdx > maxIdx) {
                currIdx = 0;
            }
        }
        return idxArr;
    }

    render(){
        const wData = this.state.loadingStatus === WeatherLoadingState.LOADED ? this.transformToDisplay(this.state.displayDailyForecast) : new Map<string, Array<string>>();
        const weatherRange = this.sliceData(this.state.currWeatherIdx, this.MAX_WEATHER_DAYS_DISPLAY, this.MAX_WEATHER_DAYS-1);
        const displayDailyForecast = this.state.displayDailyForecast;
        const hourlyForecastBtnColor = displayDailyForecast ? "default" : "primary";
        const dailyForecastBtnColor = displayDailyForecast ? "primary" : "default";
        const weatherTable = this.state.loadingStatus === WeatherLoadingState.LOADED ?
           <table className="weatherTable">
               <tbody>
                    <tr key="dateKey" className="dataRow">
                    {   weatherRange.map(idx => { return <td key={"date"+idx}>{wData.get("date")![idx]}</td>})  }
                   </tr>
                   <tr key="iconKey" className="iconRow">
                       {   weatherRange.map(idx => { return <td key={"icon"+idx}><img src={wData.get("icon")![idx]} alt=""/></td>})  }
                   </tr>
                    <tr key="tempKey" className="tempRow">
                        {weatherRange.map(idx => {
                                return <td key={"temp"+idx}>
                                    <p className="maxTemp">{wData.get("temp_max")![idx]}</p>
                                    <p> / </p>
                                    <p className="minTemp">{wData.get("temp_min")![idx]}</p>
                                </td>
                            })
                        }
                    </tr>
                   <tr key="descrKey">
                        {   weatherRange.map(idx => { return <td key={"descr"+idx}>{wData.get("descr")![idx]}</td>})  }
                   </tr>
                   <tr key="rainfallKey">
                        {  weatherRange.map(idx => { return <td key={"rain"+idx}>
                            <div>
                                <BeachAccessIcon/>
                                {wData.get("rainfall")![idx]}
                            </div>
                        </td>}) }
                   </tr>
                   <tr key="humidityKey">
                        { weatherRange.map(idx => { return <td key={"humid"+idx}>
                            <div>
                                <InvertColorsIcon/>
                                {wData.get("humidity")![idx]}
                            </div>
                        </td>})  }
                    </tr>
               </tbody>
            </table> : <p>Loading...</p>;
    
        return(
            <div className="weatherWrapper">
                <div className="weatherHeader">
                    <p>Weather Information for {this.props.locName}:</p>
                    <div className="weatherControls">
                        <Button 
                            variant="contained" 
                            color={dailyForecastBtnColor}
                            onClick={async () => {this.loadWeatherData(true);}}>Next 7 days</Button>
                        <Button 
                            variant="contained" 
                            color={hourlyForecastBtnColor}
                            onClick={async () => {this.loadWeatherData(false);}}>Today</Button>
                    </div>
                </div>
                <div className="weatherContentWrapper">
                    {this.state.loadingStatus === WeatherLoadingState.LOADING && 
                        <CircularProgress/>
                    }
                    {this.state.loadingStatus === WeatherLoadingState.ERROR && 
                        <p>Error loading weather information !</p>
                    }
                    {this.state.loadingStatus === WeatherLoadingState.LOADED &&
                        <div className="weatherContent">
                            <IconButton aria-label="previous weather entry" onClick={this.changeWeatherIdx.bind(this, false)}>
                                <SvgIcon component={ArrowBackIosIcon} style={{ fontSize: 20 }} />
                            </IconButton>
                            {weatherTable}
                            <IconButton aria-label="next weather entry" onClick={this.changeWeatherIdx.bind(this, true)}>
                                <SvgIcon component={ArrowForwardIosIcon} style={{ fontSize: 20 }} />
                            </IconButton>
                        </div>
                    }
                </div>
            </div>
        );
    }
}
export default WeatherDetails;