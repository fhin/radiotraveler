import RequestHandler from "../Utils/Request/requestHandler";
import WeatherForecast from "../Utils/ResponseModels/weatherForecast";
import { UnitType } from "../Utils/ResponseModels/weatherForecast";
import { RequestType } from "../Utils/Request/requestConstants";
import Request from "../Utils/Request/request";
import RequestConstants from "../Utils/Request/requestConstants";

class WeatherFetcher {
    private _requestHandler : RequestHandler;

    constructor(){
        this._requestHandler = new RequestHandler();
    }

    public fetchForecasts(latitude : number, longitude : number, unitType: UnitType, isDaily : boolean) : Promise<Array<WeatherForecast>> {
        return new Promise(async (resolve, reject) => {
            try {
                var forecastRequest : Request = isDaily ? this._requestHandler.getRequestForType(RequestType.DAILY_WEATHER_FORECAST)
                : this._requestHandler.getRequestForType(RequestType.HOURLY_WEATHER_FORECAST);
                forecastRequest.addActualParameter("latitude", latitude.toString());
                forecastRequest.addActualParameter("longitude", longitude.toString());
                forecastRequest.addActualParameter("useMetric", unitType === UnitType.METRIC ? true : false);

                resolve(await this._requestHandler
                    .sendRequest(forecastRequest)
                    .then(response => this.parseResponse(response, isDaily))
                );
            }
            catch (err) {
                reject("Error while fetching weatherForecasts: " + err + "\n");
            }
        });
    }

    private forecastFilter<TValue>(value: TValue | null) : value is TValue {
		if (value === null || value === undefined) return false;
		return true;
	}

    private parseResponse(jsonResponse : string, isDailyForecast : boolean) : Promise<Array<WeatherForecast>> {
        const parsePromise = new Promise<Array<WeatherForecast>>((resolve, reject) => {
            if (jsonResponse === null || jsonResponse === undefined || jsonResponse.length === 0) {
                reject("Cannot parse empty json response");
            }
            try {
                var responseObjs : Array<any> = JSON.parse(jsonResponse);
                const fetchedForecasts = responseObjs.map(responseObj => {
                    try {
                        return WeatherForecast.createFromObject(responseObj, isDailyForecast);
                    }
                    catch (err) {
                        console.log("Error while converting json to weatherStation !" + err);
                        return null;
                    }
                }).filter(this.forecastFilter);
                if (fetchedForecasts.length > RequestConstants.MAX_FORECASTS_PER_FETCH){
                    resolve(fetchedForecasts.slice(0, RequestConstants.MAX_FORECASTS_PER_FETCH - 1));
                }
                resolve(fetchedForecasts);
            }
            catch (err) {
                reject("Error while parsing weather forecast response: " + err + "\n");
            }
        });
        return parsePromise;
    }
}
export default WeatherFetcher;