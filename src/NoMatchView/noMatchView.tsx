import * as React from 'react';

class NoMatchView extends React.Component {
    render(){
        return (
            <div>
                404
            </div>
        );
    }
}
export default NoMatchView;