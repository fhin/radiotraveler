import RequestHandler from "../Utils/Request/requestHandler";
import CityInformation from "../Utils/ResponseModels/cityInformation";
import { RequestType } from "../Utils/Request/requestConstants";
import Request from "../Utils/Request/request";

class CityInfoFetcher {
    private _requestHandler : RequestHandler;

    constructor(){
        this._requestHandler = new RequestHandler();
    }

    public fetchCityInfo(cityName: string, countryName: string) : Promise<CityInformation> {
        return new Promise(async (resolve, reject) => {
            try {
                var cityInfoRequest : Request = this._requestHandler.getRequestForType(RequestType.CITY_INFORMATION)
                cityInfoRequest.addActualParameter("cityName", cityName);
                cityInfoRequest.addActualParameter("countryName", countryName);

                resolve(await this._requestHandler
                    .sendRequest(cityInfoRequest)
                    .then(response => this.parseResponse(response))
                );
            }
            catch (err) {
                reject("Error while fetching city information: " + err + "\n");
            }
        });
    }

    private parseResponse(jsonResponse : string) : Promise<CityInformation> {
        const parsePromise = new Promise<CityInformation>((resolve, reject) => {
            if (jsonResponse === null || jsonResponse === undefined || jsonResponse.length === 0) {
                reject("Cannot parse empty json response");
            }
            try {
                resolve(CityInformation.createFromObject(JSON.parse(jsonResponse)));
            }
            catch (err) {
                reject("Error while parsing city information response: " + err + "\n");
            }
        });
        return parsePromise;
    }
}
export default CityInfoFetcher;