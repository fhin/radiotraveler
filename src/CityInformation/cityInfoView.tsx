import './cityInfoView.css'
import * as React from 'react';

import CityInformation from "../Utils/ResponseModels/cityInformation";
import CityInfoFetcher from "./cityInfoFetcher";
import { SectionType } from "../Utils/ResponseModels/cityInformation";

import CircularProgress from '@material-ui/core/CircularProgress';

type CityInfoViewProps = {
    cityName: string,
    countryName: string
};

enum CityInfoStatus {
    LOADING,
    LOADED,
    ERROR
}

type CityInfoViewState = {
    loadingStatus: CityInfoStatus
    cityInformation: CityInformation
};

class CityInfoView extends React.Component<CityInfoViewProps, CityInfoViewState> {
    private dataFetcher : CityInfoFetcher;
    constructor(props: CityInfoViewProps) {
        super(props);
        this.dataFetcher = new CityInfoFetcher();
        this.state = {
            cityInformation: new CityInformation("","","",new Array<SectionType>(0)),
            loadingStatus: CityInfoStatus.LOADING
        };
    }

    async componentDidMount(){
        await this.loadCityInformation().catch(err => console.log("Error while fetching city information on startup !" + err));
    }

    private async loadCityInformation() : Promise<void> {
        try {
            this.setState({
                loadingStatus: CityInfoStatus.LOADING
            });
            var fetchedCityInfo : CityInformation = await this.dataFetcher.fetchCityInfo(this.props.cityName, this.props.countryName);
            this.setState({
                cityInformation: fetchedCityInfo,
                loadingStatus: CityInfoStatus.LOADED
            });
        }
        catch (err){
            console.error("Error while fetching forecast !" + err);
            this.setState({
                loadingStatus: CityInfoStatus.ERROR
            });
        }
    }

    private createMarkup(htmlString: string) {
        return {__html: htmlString};
    }

    render(){
        const currLoadingStatus = this.state.loadingStatus;
        const cityInfoContentClassName = currLoadingStatus !== CityInfoStatus.LOADED ? "cityInformationLoadingContent" : "cityInformationLoadedContent";
        return(
            <div className="cityInformationWrapper">
                <div className="cityInformationHeaderContent">
                    <span className="cityNameBanner">{"Information about " + this.props.cityName + ", " + this.props.countryName}</span>
                    
                </div>
                <div className="cityInformationMainContent">
                    {currLoadingStatus !== CityInfoStatus.LOADED &&
                        <div>
                                {currLoadingStatus === CityInfoStatus.LOADING && 
                                    <CircularProgress />
                                }
                                {currLoadingStatus === CityInfoStatus.ERROR &&
                                    <p>{"Error loading information about given location"}</p>
                                }
                        </div>
                    }
                    {currLoadingStatus === CityInfoStatus.LOADED &&
                        <div className={cityInfoContentClassName}>
                            <div>{this.state.cityInformation.summary}</div>
                            <ul>
                                {this.state.cityInformation.sections.map((section, idx) => {
                                    return <li key={"Section" + idx}>
                                        <div>
                                            <p dangerouslySetInnerHTML={this.createMarkup(section.sectionText)}/>
                                        </div>
                                    </li>
                                })}                        
                            </ul>
                        </div>
                    }
                </div>
            </div>
        );
    }
}
export default CityInfoView;