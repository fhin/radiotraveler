import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './index.css';
import {Switch, Route, BrowserRouter as Router} from 'react-router-dom';
import reportWebVitals from './reportWebVitals';
import HomeView from './HomeView/homeView';
import DetailViewer from './DetailView/detailView';
import NoMatchView from './NoMatchView/noMatchView';

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Switch>
        <Route exact path="/details/:title/:cityName/:countryName/:latitude/:longitude/:audioStream"
          render={(props) => <DetailViewer  {...props}/> }>
        </Route>
        <Route exact path="/home">
          <HomeView/>
        </Route>
        <Route exact path="/"></Route>
        <Route path="*">
          {/* 404 route */}
          <NoMatchView/>
        </Route>
      </Switch>
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
