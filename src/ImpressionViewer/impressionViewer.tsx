import './impressionViewer.css';
import * as React from 'react';
import AssetProvider from '../Utils/assetProvider';
import DataFetcher from '../Utils/dataFetcher';

import { IconButton, SvgIcon } from '@material-ui/core/';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';

type ImpressionViewerProps = {
    locName: string,
    locLatitude: number,
    locLongitude: number
}

type ImpressionViewerState = {
    imagesSource: string[],
    selectedImgIdx: number,
    loadedImages: boolean
}

class ImpressionViewer extends React.Component<ImpressionViewerProps, ImpressionViewerState> {
    private readonly numImagesToLoad = DataFetcher.MAX_NUM_LOCATION_IMGS;
    private readonly assetProvider : AssetProvider;
    private readonly dataFetcher: DataFetcher;

    constructor(props: ImpressionViewerProps){
        super(props);
        this.assetProvider = new AssetProvider();
        this.dataFetcher = new DataFetcher();

        this.state = {
            imagesSource: new Array<string>(this.numImagesToLoad),
            selectedImgIdx: 0,
            loadedImages: false
        };
    }

    componentDidMount(){
        this.loadImages();
    }

    private decodeImage(base64String: string): Promise<string> {
        const imageDecodePromise = new Promise<string>((resolve, reject) => {
            if (base64String === undefined || base64String.length === 0){
                reject(new Error("Could not decode given image source"));
            }
            else {
                try {
                    const decodedString = atob(base64String);
                    resolve(decodedString);
                }
                catch (err){
                    if (err instanceof DOMException){
                        reject(new Error("Could not decode given image source"));
                    }
                    else {
                        reject(err);
                    }
                }
            }
        });
        return imageDecodePromise;
    }

    private async loadImages() {
        var loadedImages = this.dataFetcher.fetchImagesForLocationName(this.props.locName)
        .then((imagesAsString) => {
            var decodePromises = new Array<Promise<string>>(this.numImagesToLoad);
            for (var imageIdx = 0; imageIdx < this.numImagesToLoad; imageIdx++){
                decodePromises[imageIdx] = this.decodeImage(imagesAsString[imageIdx]);
            }
            return Promise.allSettled(decodePromises);
        })
        .then(decodeResults => {
            return decodeResults.map((result) => {
                return result.status === "fulfilled" ? result.value : "ERR";
            });
        })
        .catch(err => {
            console.debug("Error decoding image, using default ones ! \n Reason: " + err);
            var placeHolderData = new Array<string>(this.numImagesToLoad);
            for (var idx = 0; idx < this.numImagesToLoad; idx++){
                placeHolderData[idx] = this.assetProvider.loadDecodedLocPlaceHolder();
            }
            return placeHolderData;
        });   

        const imageData = await loadedImages;
        this.setState({
            imagesSource: imageData,
            loadedImages: true
        });
        
    }

    private changeBigPicture(moveForward: boolean) {
        var currPicIdx = this.state.selectedImgIdx;
        if (currPicIdx === 0 && !moveForward){
            currPicIdx = this.numImagesToLoad-1;
        }
        else if (currPicIdx === (this.numImagesToLoad-1) && moveForward){
            currPicIdx = 0;
        }
        else {
            currPicIdx = moveForward ? currPicIdx++ : currPicIdx--;
        }
        this.setState({
            selectedImgIdx: currPicIdx
        });
    }

    render(){
        const imgs = this.state.imagesSource;
        const availableImages = new Array(this.numImagesToLoad);

        if (!this.state.loadedImages){
            console.log("Still loading images, using placeholder ones");
            for (var i = 0; i < this.numImagesToLoad; i++){
                availableImages[i] = <img src={this.assetProvider.loadDecodedLocPlaceHolder()} alt="" key={i}/>
            }
        }
        else {
            console.log("Loaded images");
            imgs.slice(0,this.numImagesToLoad).forEach((imageAsBase64String, index) => {
                availableImages[index] = <img src={imageAsBase64String} alt="" key={index}/>
            });
        }

        return(
            <div className="impressionWrapper">
                <div className="bigPicture">
                    <IconButton aria-label="previous impression" onClick={this.changeBigPicture.bind(this, false)}>
						<SvgIcon component={ArrowBackIosIcon} style={{ fontSize: 20 }} />
					</IconButton>
                     { availableImages[0] }
                     <IconButton aria-label="next impression" onClick={this.changeBigPicture.bind(this, false)}>
						<SvgIcon component={ArrowForwardIosIcon} style={{ fontSize: 20 }} />
					</IconButton>
                </div>
                <div className="pictureRangeContainer">
                    <div className="pictureRange">
                        {availableImages}
                    </div>
                </div>
            </div>
        );
    }
}
export default ImpressionViewer;
